package statuses2;

import javax.jms.*;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;


public class sendToMQ {
    
    void sendMQ(String barcode)
    {
        try{
            final ActiveMQConnectionFactory connectionFactory =
                createActiveMQConnectionFactory();
        final PooledConnectionFactory pooledConnectionFactory =
                createPooledConnectionFactory(connectionFactory);

        sendMessage(pooledConnectionFactory, barcode);

        pooledConnectionFactory.stop();
            
            
            
        }
        catch(Exception e){

        }
    }
    private void sendMessage(PooledConnectionFactory pooledConnectionFactory, String barcode) throws JMSException {

        final Connection producerConnection = pooledConnectionFactory
                .createConnection();
        producerConnection.start();

        final Session producerSession = producerConnection
                .createSession(false, Session.AUTO_ACKNOWLEDGE);

        final Destination producerDestination = producerSession
                .createQueue("RP.Details2BJ");

        final MessageProducer producer = producerSession
                .createProducer(producerDestination);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

        final TextMessage producerMessage = producerSession
                .createTextMessage(barcode);

        producer.send(producerMessage);
        System.out.print(" - mq message sent.");

        producer.close();
        producerSession.close();
        producerConnection.close();
    }

    private static PooledConnectionFactory
    createPooledConnectionFactory(ActiveMQConnectionFactory connectionFactory) {
        final PooledConnectionFactory pooledConnectionFactory =
                new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        pooledConnectionFactory.setMaxConnections(10);
        return pooledConnectionFactory;
    }

    private static ActiveMQConnectionFactory createActiveMQConnectionFactory() {
        final ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("failover:(tcp://sgeptalendrt3.fmlogistic.fr:61616,"+"tcp://sgeptalendrt4.fmlogistic.fr:61617)?randomize=false&priorityBackup=true");
        connectionFactory.setUserName("pesb");
        connectionFactory.setPassword("7WEgTYMIWE79yP4ePj0k");
        return connectionFactory;
    }
    
}
