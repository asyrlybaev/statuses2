package statuses2;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.xml.parsers.*;
import org.w3c.dom.Document;

public class Statuses2 {

public static void main(String[] args) throws Exception{
    
DocumentBuilderFactory docFact = DocumentBuilderFactory.newInstance();
DocumentBuilder docBuilder = docFact.newDocumentBuilder();
Document doc = docBuilder.newDocument();
org.w3c.dom.Element root = doc.createElement("STATUSES");
doc.appendChild(root);

SimpleDateFormat sdfToDateTime = new SimpleDateFormat();
sdfToDateTime.applyPattern("yyyy-MM-dd' 'HH:mm:ss");
SimpleDateFormat sdfBJ = new SimpleDateFormat();
sdfBJ.applyPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
SimpleDateFormat sdIDBdate = new SimpleDateFormat();
sdIDBdate.applyPattern("dd.MM.yyyy");
SimpleDateFormat sdIDBtime = new SimpleDateFormat();
sdIDBtime.applyPattern("HH:mm:ss");
List<xmlStatus> stList = new ArrayList<>();
xmlParser xp = new xmlParser();
getBarcodes bc = new getBarcodes();

//List oldlist = bc.BarcodeList();
List oldlist = new ArrayList();
oldlist.add(0, "14087131069369");

Iterator iterator = oldlist.iterator();
int length = oldlist.size()-1;
      while(iterator.hasNext()) {
      try{
              String barcode0 = iterator.next().toString();
              System.out.println(barcode0+" - processing... " + length + " left.");
              length--;
              getStatusesFromRP gsfrp = new getStatusesFromRP();
              StringBuffer sb = gsfrp.request(barcode0);
              java.util.Date latest = bc.getlatest(barcode0);
     //         System.out.println("Latest   "+latest.toString());
            
            stList = xp.getstList(sb, latest);
            for(xmlStatus st:stList){


                sendSMS ssms = new sendSMS();
                sendToBJ stbj = new sendToBJ();
                getBarcodes gb = new getBarcodes();
                
                String OrdNum = "";
                String TelNum = "";
                String ShipNum = "";
                Integer DBflag = 0;
                int flag = ssms.getFlag(st.typeid.toString(), st.attrid.toString());
                List details = gb.getDetails(barcode0);
                OrdNum = details.get(0).toString();
                TelNum = details.get(1).toString();
                ShipNum = details.get(2).toString();
                try{DBflag = Integer.parseInt(details.get(3).toString());}catch(Exception e){DBflag = 0;}
                    
                switch (DBflag)
                {
                    case 3:
                    case 8:
                        {System.out.print(st.date+" " +st.typeid+ " "+st.attrid+ " - Flag: "+flag+" ("+stbj.getRemark(flag)+") - Заказ был закрыт");}
                        break;
                    case 4:
                    case 5: 
                    case 7:
                        if(flag==8 || flag==2){
                        flag=8;
                        DBflag=8;
                        xp.saveToOms(st.barcode, st.date, st.typeid, st.attrid);
                        xp.saveDBStatus(flag, barcode0);
                        System.out.print(st.date+" " +st.typeid+ " "+st.attrid+ " - Flag: "+flag+" ("+stbj.getRemark(flag)+")");
                        stbj.sendRequest(ShipNum, sdfBJ.format(st.date), flag);
                    }
                    else{
                    xp.saveToOms(st.barcode, st.date, st.typeid, st.attrid);
                    flag = 12;
                    DBflag = 12;
                    System.out.print(st.date+" " +st.typeid+ " "+st.attrid+ " - Flag: "+flag+" ("+stbj.getRemark(flag)+")");
                    stbj.sendRequest(ShipNum, sdfBJ.format(st.date), flag);
                    }
                        break;
                        
                    default:
                        if (flag == 1){
                        int Codtra = gb.getCodtra(OrdNum);
                        Boolean smsstatus = ssms.sendSms(flag, OrdNum, Codtra, TelNum, barcode0);
                        String sms = "";
                        xp.saveSmsStatus(flag, barcode0);
                        if(smsstatus = true)
                        {   xp.saveToOms(st.barcode, st.date, st.typeid, st.attrid);
                            xp.saveSmsStatus(flag, barcode0);
                        sms = " - SMS " + flag + " to " + TelNum;
                       }
                        else {sms = "Couldn't send SMS";}
                        
                        System.out.print(st.date+" " +st.typeid+ " "+st.attrid+ " - Flag: "+ flag+" ("+stbj.getRemark(flag)+") "+sms);
                        stbj.sendRequest(ShipNum, sdfBJ.format(st.date), flag);
                    }
                        else if (flag == 2){
                            int Codtra = gb.getCodtra(OrdNum);
                        Boolean smsstatus = ssms.sendSms(flag, OrdNum, Codtra, TelNum, barcode0);
                        String sms = "";
                        xp.saveSmsStatus(flag, barcode0);
                        if(smsstatus = true)
                        {   xp.saveToOms(st.barcode, st.date, st.typeid, st.attrid);
                            xp.saveSmsStatus(flag, barcode0);
                            sendToMQ smq = new sendToMQ();
                            smq.sendMQ(barcode0);
                            sms = " - SMS " + flag + " to " + TelNum;
                       }
                        else {sms = "Couldn't send SMS";}

                        System.out.print(st.date+" " +st.typeid+ " "+st.attrid+ " - Flag: "+ flag+" ("+stbj.getRemark(flag)+") "+sms);
                        stbj.sendRequest(ShipNum, sdfBJ.format(st.date), flag);
                    }
                        else{
                        xp.saveToOms(st.barcode, st.date, st.typeid, st.attrid);
                        xp.saveDBStatus(flag, barcode0);
                        System.out.print(st.date+" " +st.typeid+ " "+st.attrid+ " - Flag: "+flag+" ("+stbj.getRemark(flag)+")");
                        stbj.sendRequest(ShipNum, sdfBJ.format(st.date), flag);
                        }
                        break;
                }
                
                switch(DBflag)
                {
                    case 0:
                    case 8:
                        break;
                    default:
                        try{
                                        org.w3c.dom.Element req = doc.createElement("Request");
					root.appendChild(req);
					org.w3c.dom.Element ordNum = doc.createElement("Order");
					ordNum.appendChild(doc.createTextNode(OrdNum));
					req.appendChild(ordNum);

					org.w3c.dom.Element descr = doc.createElement("Descript");
					descr.appendChild(doc.createTextNode(stbj.getRemark(flag)));
					req.appendChild(descr);

					org.w3c.dom.Element dateStat = doc.createElement("Date-status");
					dateStat.appendChild(doc.createTextNode(sdIDBdate.format(st.date)));
					req.appendChild(dateStat);

					org.w3c.dom.Element timeStat = doc.createElement("Time-status");
					timeStat.appendChild(doc.createTextNode(sdIDBtime.format(st.date)));
					req.appendChild(timeStat);

					if (flag == 2) 
					{
						org.w3c.dom.Element dateAct = doc.createElement("Actual-date");
						dateAct.appendChild(doc.createTextNode(sdIDBdate.format(st.date)));
						req.appendChild(dateAct);

						org.w3c.dom.Element timeAct = doc.createElement("Actual-time");
						timeAct.appendChild(doc.createTextNode(sdIDBtime.format(st.date)));
						req.appendChild(timeAct);
					}
					
						org.w3c.dom.Element track = doc.createElement("Tracking-number");
						track.appendChild(doc.createTextNode(barcode0));
						req.appendChild(track);
					
                    }
                    catch(Exception e){}
                        break;
                }

            }

}
catch(Exception e){System.out.println(e);}
}

xp.makeStatusXML(doc);

System.out.println("Press a key to exit...");
Scanner scanner = new Scanner(System.in); 
scanner.nextLine();
}
}