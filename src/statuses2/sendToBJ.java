package statuses2;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class sendToBJ {
    
    void sendRequest(String ShipNum, String Date, int flag)
	{
                 final String xml = "<?xml version=\"1.0\" ?>\r\n"
			+ "<status xmlns=\"http://www.kewill.com/logistics/klic/status\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" type=\"tag\">\r\n"
			+ "	<ediCustomerNumber>99999998</ediCustomerNumber>\r\n"
			+ "	<ediCustomerDepartment>SLTL</ediCustomerDepartment>\r\n" + "	<ediParm1>4</ediParm1>\r\n"
			+ "	<ediParm2>f</ediParm2>\r\n" + "	<transmitter>RP</transmitter>\r\n" + "	<receiver>RUDOM</receiver>\r\n"
			+ "	<ediReference/>\r\n" + "	<internalNumber>"+ShipNum+"</internalNumber>\r\n"
			+ "	<ediFunction1>2</ediFunction1>\r\n" + "	<fileHeader type=\"tag\">\r\n"
			+ "		<trackingAndTracing type=\"tag\">\r\n" + "      <dateTimeZone>"+Date+"</dateTimeZone>\r\n"
			+ "      <code>"+this.getStatus(flag)+"</code>\r\n" + "      <remark_1>"+this.getRemark(flag)+"</remark_1>\r\n" + "    </trackingAndTracing>"
			+ "</fileHeader>\r\n" + "</status>";

		try
		{
		URL obj = new URL("http://hmkchain:8060/klic/status/");

		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");

		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
                
		InputStream is = new ByteArrayInputStream(xml.getBytes());
			int c = 0;
			byte[] buf = new byte[8192];
			while ((c = is.read(buf, 0, buf.length)) > 0)
			{
				os.write(buf, 0, c);
				os.flush();
			}
                            os.close();
                            is.close();
                int responseCode = con.getResponseCode();
		System.out.println(" - BJ Response: " + responseCode);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
                
                
        }
    
   String getStatus(int flag)
    {
       String Status;
    switch (flag)
{
        case 0:
		Status = "ORDTRANSIT";
		break;
	case 1:
		Status = "ORDPICKUP";
		break;
	case 2:
		Status = "ORDDELIVER";
		break;
        case 3:
		Status = "ORD_OK";
		break;
        case 4:
		Status = "ORDRETERN";
		break;
        case 5:
		Status = "ORDRETERN";
		break;
        case 6:
                Status = "ORDALLERT";
		break;
        case 7:
                Status = "ORDRETERN";
		break;
        case 8:
                Status = "ORDRET_OK";
		break;
        case 9:
                Status = "ORDTRANSIT";
		break;
        case 10:
                Status = "ORDTRANSIT";
		break;
        case 11:
                Status = "ORDTRANSIT";
		break;
        case 12:
                Status = "ORDTRANSIT";
		break;
                
         case 13:
                Status = "ORDTRANSIT";
		break;
        case 14:
                Status = "ORDPICKUP";
		break;
	default:
		Status = "TEST";
		break;
    }
    return Status;
}

   String getRemark (int flag){
        String Status;
        switch (flag)
{
        case 0:
                Status = "Заказ в пути";
		break;
	case 1:
                Status = "Принято в отделении связи";
		break;
	case 2:
                Status = "Заказ готов к выдаче";
		break;
        case 3:
                Status = "Заказ выдан получателю";
		break;
        case 4:
                Status = "Заказ передан на возврат";
		break;
        case 5:
                Status = "Истек срок хранения";
		break;
        case 6:
                Status = "Проблема с заказом";
		break;
        case 7:
                Status = "Отказ адресата";
		break;
        case 8:
                Status = "Вернулось отправителю";
		break;
        case 9:
                Status = "Прибыло в сортировочный центр";
		break;
        case 10:
                Status = "Покинуло сортировочный центр";
		break;
        case 11:
                Status = "Сортировка";
		break;
        case 12:
                Status = "Возврат в пути";
		break;
        case 13:
                Status = "Досылка/Возврат";
		break;
        case 14:
                Status = "Присвоен трек-номер";
		break;
	default:
                Status = "Test";
		break;
}
        return Status;
    }
}

